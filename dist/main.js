"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.searchAndReplaceInFile = searchAndReplaceInFile;
exports.loadYamlFile = loadYamlFile;
exports.writeYamlFile = writeYamlFile;
exports.exists = exists;
exports.killProcess = killProcess;
exports.loadJsonFile = loadJsonFile;
exports.writeJsonFile = writeJsonFile;
exports.getPageContent = getPageContent;
exports.downloadFile = downloadFile;
exports.isElectronPackaged = exports.isDev = void 0;

var _util = require("util");

var childProcess = _interopRequireWildcard(require("child_process"));

var fs = _interopRequireWildcard(require("fs"));

var jsYaml = _interopRequireWildcard(require("js-yaml"));

var _request = _interopRequireDefault(require("request"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

const exec = (0, _util.promisify)(childProcess.exec);
const request = (0, _util.promisify)(_request.default);

function searchAndReplaceInFile(fileName, oldText, newText) {
  let fileContent = fs.readFileSync(fileName, 'utf8');
  const oldTextRegex = new RegExp(oldText, 'g');
  fileContent = fileContent.replace(oldTextRegex, newText);
  fs.writeFileSync(fileName, fileContent, 'utf8');
}

function loadYamlFile(yamlFile) {
  return jsYaml.safeLoad(fs.readFileSync(yamlFile, 'utf8'));
}

function writeYamlFile(yamlFile, yamlContent) {
  fs.writeFileSync(yamlFile, jsYaml.safeDump(yamlContent), 'utf8');
}

function exists(contentPath) {
  try {
    return fs.statSync(contentPath).isFile() || fs.statSync(contentPath).isDirectory();
  } catch {
    return false;
  }
}

async function killProcess(processName) {
  return exec(`taskkill /im ${processName} /t /f`);
}

function loadJsonFile(filePath) {
  return JSON.parse(fs.readFileSync(filePath, 'utf8'));
}

function writeJsonFile(filePath, jsonContent) {
  fs.writeFileSync(filePath, JSON.stringify(jsonContent));
}

async function getPageContent(url, isJson) {
  const options = {
    url: url,
    headers: {
      'User-Agent': 'Mozilla/5.0'
    },
    json: isJson
  };
  const response = await request(options);
  return response.body;
}

async function downloadFile(url, fileDestination) {
  const options = {
    url: url,
    headers: {
      'User-Agent': 'Mozilla/5.0'
    },
    encoding: null
  };
  const response = await request(options);
  fs.writeFileSync(fileDestination, response.body);
}

const isDev = process.env.ELECTRON_ENV !== 'production';
exports.isDev = isDev;
const isElectronPackaged = process.mainModule.filename.indexOf('app.asar') !== -1;
exports.isElectronPackaged = isElectronPackaged;
import { promisify } from 'util';
import * as childProcess from 'child_process';
import * as fs from 'fs';
import * as jsYaml from 'js-yaml';
import req from 'request';

const exec = promisify(childProcess.exec);
const request = promisify(req);

export function searchAndReplaceInFile(fileName, oldText, newText) {
  let fileContent = fs.readFileSync(fileName, 'utf8');

  const oldTextRegex = new RegExp(oldText, 'g');
  fileContent = fileContent.replace(oldTextRegex, newText);

  fs.writeFileSync(fileName, fileContent, 'utf8');
}

export function loadYamlFile(yamlFile) {
  return jsYaml.safeLoad(fs.readFileSync(yamlFile, 'utf8'));
}

export function writeYamlFile(yamlFile, yamlContent) {
  fs.writeFileSync(yamlFile, jsYaml.safeDump(yamlContent), 'utf8');
}

export function exists(contentPath) {
  try {
    return fs.statSync(contentPath).isFile() || fs.statSync(contentPath).isDirectory();
  } catch {
    return false;
  }
}

export async function killProcess(processName) {
  return exec(`taskkill /im ${processName} /t /f`);
}

export function loadJsonFile(filePath) {
  return JSON.parse(fs.readFileSync(filePath, 'utf8'));
}

export function writeJsonFile(filePath, jsonContent) {
  fs.writeFileSync(filePath, JSON.stringify(jsonContent));
}

export async function getPageContent(url, isJson) {
  const options = {
    url: url,
    headers: { 'User-Agent': 'Mozilla/5.0' },
    json: isJson
  };

  const response = await request(options);
  return response.body;
}

export async function downloadFile(url, fileDestination) {
  const options = {
    url: url,
    headers: { 'User-Agent': 'Mozilla/5.0' },
    encoding: null
  };

  const response = await request(options);
  fs.writeFileSync(fileDestination, response.body);
}

export const isDev = process.env.ELECTRON_ENV !== 'production';

export const isElectronPackaged = process.mainModule.filename.indexOf('app.asar') !== -1;

# Introduction

This is simply my personal compilation of wrappers and snippets

# Installation

```
npm i "@lovely.sh/wrappers"
```

# Usage

```
const wrapper = require('@lovely.sh/wrappers');

const resources = path.join(__dirname, 'resources');

// searchAndReplaceInFile
const textFile = path.join(resources, 'demo.txt');
wrapper.searchAndReplaceInFile(textFile, 'old text', 'new text');

// loadYaml ans writeYaml
const yamlFile = path.join(resources, 'demo.yaml');
const yamlContent = wrapper.loadYamlFile(yamlFile);

console.log(yamlContent.text);
yamlContent.version = '1.1.1';

wrapper.writeYamlFile(yamlFile, yamlContent);

// isExist
isExist = wrapper.exists(textFile);
if (isExist) {
  console.log('File/folder exist');
} else {
  console.log("File/folder don't exist");
}

// loadJsonFile and writeJsonFile
const jsonFile = path.join(resources, 'demo.json');
jsonContent = wrapper.loadJsonFile(jsonFile);

console.log(jsonContent.text);
jsonContent.version = '1.1.1';

wrapper.writeJsonFile(jsonFile, jsonContent);

// killProcess
try {
  const killProcess = await wrapper.killProcess('chrome.exe');
  console.log(killProcess.stdout);
} catch (error) {
  console.log(error);
}

// getPageContent()
try {
  console.log(await wrapper.getPageContent('https://httpbin.org/get', true));
} catch (error) {
  console.log(error);
}

// downloadFile
try {
  await wrapper.downloadFile('https://upload.wikimedia.org/wikipedia/commons/d/d2/Codinglogo.jpg', 'dev-image.jpg');
  console.log('Success to download file');
} catch (error) {
  console.log(error);
}

// isDev and isElectronPackaged
if (wrapper.isDev && !wrapper.isElectronPackaged) {
  console.log('Running on development');
} else {
  console.log('Running on production');
}
```
